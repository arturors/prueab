package com.techu.apitechudb;

import com.techu.apitechudb.models.ProductModel;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;

@SpringBootApplication
public class ApitechudbApplication {

	public static ArrayList<ProductModel> productModels;

	public static void main(String[] args) {
		SpringApplication.run(ApitechudbApplication.class, args);

		ApitechudbApplication.productModels = ApitechudbApplication.getTestData();
	}

	private static ArrayList<ProductModel> getTestData() {

		ArrayList<ProductModel> productModels = new ArrayList<>();

		productModels.add(
				new ProductModel(
						"1"
						,"Producto 1"
						,10
				)
		);

		productModels.add(
				new ProductModel(
						"2"
						,"Producto 2"
						,20
				)
		);

		productModels.add(
				new ProductModel(
						"3"
						,"Producto 3"
						,30
				)
		);

		return productModels;
	}


}
