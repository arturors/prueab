package com.techu.apitechudb.repositories;

import com.techu.apitechudb.ApitechudbApplication;
import com.techu.apitechudb.models.ProductModel;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class ProductRepository {

    public List<ProductModel> findAll() {
        System.out.println("findAll en ProductRepository");
        return ApitechudbApplication.productModels;
    }

    public ProductModel save(ProductModel product) {
        System.out.println("Save en ProductRepository");
        ApitechudbApplication.productModels.add(product);
        return product;
    }

    public Optional<ProductModel> findById(String id) {
        System.out.println("findById en productRepository");
        Optional<ProductModel> result = Optional.empty();

        for (ProductModel productInList : ApitechudbApplication.productModels){
            if (productInList.getId().equals(id)){
                System.out.println("Producto encontrado en findById");
                result = Optional.of(productInList);
            }
        }
        return result;
    }

    public ProductModel update (ProductModel product) {
        System.out.println("update en ProductRepository");

        Optional<ProductModel> productToUpdate = this.findById(product.getId());

        if (productToUpdate.isPresent() == true){
            System.out.println("producto encontrado en if de UPDATE");
            ProductModel productFromList = productToUpdate.get();
            System.out.println("el precio FromList es " + productFromList.getPrice());
            System.out.println("el precio ToUpdate es " + product.getPrice());
            productFromList.setDesc(product.getDesc());
            productFromList.setPrice(product.getPrice());
        }
        return product;
    }

    public void delete(ProductModel product) {
        System.out.println("delete en ProductRepository");
        System.out.println("borrando producto");
        ApitechudbApplication.productModels.remove(product);
    }
}
